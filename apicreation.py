from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional

app = FastAPI()

#define path operation or route /posts
@app.get("/posts")
def get_post():
    return {"data":[{
    "Title": "Welcome to our blog",
    "Content": "This is just the beginning of a beautiful friendship ",
    "Author": "Ivan Vukotic",
},
{
    "Title": "Top 10 best places in Luxembourg",
    "Content": "Although a small country it has lot to offer",
    "Author": "Xavier Bettel",
}]}

class BlogPosts(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = 0


@app.post("/createPosts")
def create_posts(new_cont: BlogPosts):
    print(new_cont)
    print(new_cont.title)
    print(new_cont.content)
    print(new_cont.rating)
    return {"message": f"New blog posts added with title: {new_cont.title}; and its content is {new_cont.content} and the rating is {new_cont.rating} "}