#Initialize the class and make the necesarry functions 
class BankAccount():
    def __init__(self, number, owner, balance):
        self.num = number
        self.own = owner
        self.bala = balance
        
    def deposit(self, add):
        self.bala += add
        
        
    def withdrawal(self, take):
        if self.bala >= take:
            self.bala -= take
        else:
            print("You cannot take this amount of money")
        
        
    def agios(self):
        self.bala = self.bala * 0.95
        
        
    def display(self):
        print(f"This is account of {self.own}, and his account number is {self.num}, and the balance is {self.bala}")
#Enter the data and initialize the customer object from Class BankAccount()
name = input("Please input name of the customer")
number = float(input("Please enter account number"))
balance = float(input("Please enter your balance"))
 
customer = BankAccount(name, number, balance)
#This is a loop so that the program doesn't stop and we can do until we write exit
still_enter = True
while still_enter:
    r = input("Please enter what do you want to do: deposit,withdrawal,agios") 
#if the user enters deposit it changes the balance and exits if after the user writes exit
    if r.lower() == "deposit":
        money = float(input("Tell how much you want to deposit"))
        customer.deposit(money)
        customer.display()
        b = input("Do you want to exit")
        if b == "exit":
            still_enter = False
#if the user enters withdrawal it changes the balance if the customer has enough money on the balanceand exits if after the user writes exit
    elif r.lower() == "withdrawal":
        money = float(input("Tell how much you want to deposit"))
        customer.withdrawal(money)
        customer.display()
        b = input("Do you want to exit")
        if b == "exit":
            still_enter = False
#Just uses agios functions if the user enters it 
    elif r.lower() == "agios":
        customer.agios()
        customer.display()
        b = input("Do you want to exit")
        if b == "exit":
            still_enter = False
    else:
        print("You have entered a wrong option")

           